# https://www.geeksforgeeks.org/queue-data-structure/
# https://www.digitalocean.com/community/tutorials/queue-in-c


class Queue:
    """FIFO"""

    def __init__(self) -> None:
        self._data = []

    def enqueue(self, __val):
        self._data.append(__val)
        return self

    def dequeue(self):
        if len(self._data) == 0:
            raise Exception("Underflow")

        return self._data.pop(0)

    def size(self):
        return len(self._data)

    def isEmpty(self):
        return self.size() == 0

    def __str__(self) -> str:
        return f"""
Queue(
    data    : {self._data}
    size    : {self.size()}
    isEmpty : {self.isEmpty()}
)
""".strip()

    def __repr__(self) -> str:
        return str(self)


if __name__ == "__main__":
    q = Queue()
    print(q.enqueue(10).enqueue(100).enqueue(10 ** 5))
    while not q.isEmpty():
        print(q.dequeue())

    print(q)
