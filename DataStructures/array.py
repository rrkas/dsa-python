# https://www.geeksforgeeks.org/array-data-structure/

import sys


class Array:
    def __init__(self, dtype: type) -> None:
        if not isinstance(dtype, type):
            raise TypeError(f"Expected type or class, got {dtype}")

        self._dtype = dtype
        self._data: list[self._dtype] = []

    def append(self, __v):
        if not isinstance(__v, self._dtype):
            raise TypeError(f"value ({__v}) is not of type {self._dtype}")

        self._data.append(__v)
        return self

    def insert(self, __idx: int, __v):
        if not isinstance(__v, self._dtype):
            raise TypeError(f"value ({__v}) is not of type {self._dtype}")

        self._data.insert(__idx, __v)
        return self

    def pop(self, __idx: int = -1):
        return self._data.pop(__idx)

    def remove(self, __val):
        self._data.remove(__val)
        return self

    def index(self, __val, __start: int = 0, __stop: int = sys.maxsize):
        return self._data.index(__val, __start, __stop)

    def size(self):
        return len(self._data)

    def __getitem__(self, __val: int | slice):
        return self._data[__val]

    def __str__(self) -> str:
        return f"""Array(
    data    : {self._data}, 
    size    : {self.size()}, 
    dtype   : {self._dtype.__name__}
)""".strip()

    def __repr__(self) -> str:
        return str(self)


if __name__ == "__main__":
    arr = Array(str)
    print(arr.append("3"))
    print(
        arr.insert(0, "100")
        .insert(1, "101")
        .insert(2, "102")
        .append("200")
        .append("800")
        .append("1000")
    )
    print(arr.pop(), arr.pop(0))
    print(arr)
    print(arr.remove("3"))
    print(arr.index("102"))
    print(arr[::-1])
