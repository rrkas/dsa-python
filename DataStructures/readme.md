# Data Structures

https://www.geeksforgeeks.org/data-structures/

1. Linear DS
    1. Static DS
        1. [Array](./array.py)
    1. Dynamic
        1. Queue
            1. Priority Queue
            1. Dequeue
        1. Stack
        1. Linked List
            1. Singly linked list
            1. Circular linked list
            1. Doubly linked list
1. Non-linear DS
    1. Tree
        1. Binary Tree
            1. Full Binary Tree
            1. Degenerate Binary Tree
            1. Skewed Binary Trees
        1. Binary Search Tree (BST)
        1. Heap
    1. Graph
