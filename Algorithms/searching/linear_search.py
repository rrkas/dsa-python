# https://www.geeksforgeeks.org/linear-search/

import sys
from pathlib import Path

sys.path.append(Path(__file__).parent.parent)

from DataStructures.array import Array


def linear_search(arr: Array, __val):
    for i in range(arr.size()):
        if arr[i] == __val:
            return i

    return None
